package com.zkzong.mongodb.hosting.dao;

import com.zkzong.mongodb.hosting.model.Hosting;

public interface HostingDao {

	void save(Hosting hosting);

}