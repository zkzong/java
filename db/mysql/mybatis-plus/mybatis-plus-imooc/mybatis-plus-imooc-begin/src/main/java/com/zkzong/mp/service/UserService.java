package com.zkzong.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zkzong.mp.entity.User;

public interface UserService extends IService<User> {
}
