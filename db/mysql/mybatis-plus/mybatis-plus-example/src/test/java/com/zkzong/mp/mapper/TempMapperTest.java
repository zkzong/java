//package com.zkzong.mp.mapper;
//
//import com.zkzong.mp.entity.Temp;
//import com.zkzong.mp.mapper.test.TempMapper;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
///**
// * @Author: zkzong
// * @Date: 2018.9.8
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class TempMapperTest {
//
//    @Autowired
//    private TempMapper tempMapper;
//
//    @Test
//    public void getById() {
//        Temp temp = tempMapper.getById(1);
//        System.out.println(temp);
//    }
//
//    @Test
//    public void selectById() {
//        Temp temp = tempMapper.selectById(1);
//        System.out.println(temp);
//    }
//
//}
