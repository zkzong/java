package com.zkzong.mp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

    private Integer pageNum;
    private Integer pageSize;

}
