package com.zkzong.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkzong.mp.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
