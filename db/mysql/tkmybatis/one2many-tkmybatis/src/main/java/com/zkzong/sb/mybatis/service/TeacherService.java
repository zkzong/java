package com.zkzong.sb.mybatis.service;

import com.zkzong.sb.mybatis.domain.Teacher;

import java.util.List;

/**
 * @Author: zkzong
 * @Date: 2018.9.5
 */
public interface TeacherService {
    List<Teacher> getTeachers();
}
