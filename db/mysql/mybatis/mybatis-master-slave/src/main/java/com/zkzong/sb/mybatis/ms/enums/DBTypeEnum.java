package com.zkzong.sb.mybatis.ms.enums;

public enum DBTypeEnum {
    MASTER, SLAVE1, SLAVE2;
}
