package com.zkzong.sb.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableAnnotationApplication {

	public static void main(String[] args) {
		SpringApplication.run(TableAnnotationApplication.class, args);
	}
}
