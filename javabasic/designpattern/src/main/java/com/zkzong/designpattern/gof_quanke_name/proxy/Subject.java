package com.zkzong.designpattern.gof_quanke_name.proxy;

/**
 * Created by Zong on 2016/11/23.
 */
public abstract class Subject {
    public abstract void request();
}
