package com.zkzong.annotation;

public @interface DefaultValue {
    String value();
}
