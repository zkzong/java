package com.zkzong.threadlocal.demo;

public interface ProductService {
    void updateProductPrice(long productId, int price);
}
