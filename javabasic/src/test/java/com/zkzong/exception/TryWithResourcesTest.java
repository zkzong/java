package com.zkzong.exception;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @Author: Zong
 * @Date: 2018/12/9
 */
public class TryWithResourcesTest {

    @Test
    public void tryResource() throws IOException {
        String filePath = "file/github.txt";
        TryWithResources tr = new TryWithResources();
        tr.tryResource(filePath);
    }

}