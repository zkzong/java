package com.zkzong.reflect.example.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Man {
    private String name;
    private Integer age;
}
