package com.zkzong.thinkinginjava.util;

/**
 * Created by Zong on 2016/8/25.
 */
public interface Generator<T> {
    T next();
}
