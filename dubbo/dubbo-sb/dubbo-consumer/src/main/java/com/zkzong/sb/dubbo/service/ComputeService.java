package com.zkzong.sb.dubbo.service;

/**
 * Created by Zong on 2017/5/12.
 */
public interface ComputeService {
    Integer add(int a, int b);
}
