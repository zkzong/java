package com.zkzong.dubbox.service;

/**
 * Created by Zong on 2017/3/30.
 */
public interface DemoService {
    String sayHello(String name);
}
