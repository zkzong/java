package com.zkzong.dubbo.service;

/**
 * Echo Service.
 */
public interface EchoService {

    String echo(String message);
}
