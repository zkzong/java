## thirdparty

### apache commons

### common

公共

### elastic-job
分布式定时任务

### guava

### json

#### fastjson
#### gson
#### jackson
#### json-lib
#### json-perform-test
使用JMH基准测试

### lombok
IDE需要安装lombok插件，否则编译报错

### mockito

### okhttp

### orika
根据字段名称赋值，类型不一样也可以赋值。

### pdf

#### itext

#### pdf2image
使用pdfbox，把pdf转换成图片

可以把整个pdf转成一张图片，也可以每页pdf转成一张图片

### pinyin4j

### poi

### snowflake
雪花算法
+ struqt-unique-id

### spring
spring工具类
+ BeanUtils

### trove

使用lombok