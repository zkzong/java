package com.zkzong.qdox.service;

/**
 * @Author: zong
 * @Date: 2021/11/25
 */
public interface QdoxService {

    String getQdox();

    String getName();

}
