package com.zkzong.qdox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: zong
 * @Date: 2021/11/25
 */
@SpringBootApplication
public class QdoxApplication {
    public static void main(String[] args) {
        SpringApplication.run(QdoxApplication.class, args);
    }
}
