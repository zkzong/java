package com.zkzong.testng;

import org.testng.annotations.Test;

/**
 * @author zkzong
 * @date 2017/11/11
 */
public class TestDBConnection {

    @Test
    public void runOtherTest1() {
        System.out.println("@Test - runOtherTest1");
    }

    @Test
    public void runOtherTest2() {
        System.out.println("@Test - runOtherTest2");
    }
}
