package com.zkzong.ss.repository;

import com.zkzong.ss.entity.User;

public interface UserRepository extends CommonRepository<User, Long> {
}
