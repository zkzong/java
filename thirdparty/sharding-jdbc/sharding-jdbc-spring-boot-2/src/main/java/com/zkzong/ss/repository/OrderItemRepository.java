package com.zkzong.ss.repository;

import com.zkzong.ss.entity.OrderItem;

public interface OrderItemRepository extends CommonRepository<OrderItem, Long> {
}
