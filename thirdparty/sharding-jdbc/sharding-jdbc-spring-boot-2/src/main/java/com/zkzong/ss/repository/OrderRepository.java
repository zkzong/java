package com.zkzong.ss.repository;

import com.zkzong.ss.entity.Order;

public interface OrderRepository extends CommonRepository<Order, Long> {
}
