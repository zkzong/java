package com.zkzong.ss.repository;

import com.zkzong.ss.entity.Address;

public interface AddressRepository extends CommonRepository<Address, Long> {
}
