package com.zkzong.sj.repository;

import com.zkzong.sj.entity.Other;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtherRepository extends JpaRepository<Other, Long> {

}
