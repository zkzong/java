package com.zkzong.sj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaComplexApplication {

    public static void main(final String[] args) {
        SpringApplication.run(JpaComplexApplication.class, args);
    }

}
