package com.zkzong.sj.entity;

import lombok.Data;

@Data
public class Other {

    private long id;
    private String userName;
    private int age;

}
