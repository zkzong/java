
package com.zkzong.sj4.entity;

import lombok.Data;

@Data
public class Address {

    private Long addressId;

    private String addressName;

}
