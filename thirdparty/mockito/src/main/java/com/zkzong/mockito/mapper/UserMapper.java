package com.zkzong.mockito.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkzong.mockito.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
