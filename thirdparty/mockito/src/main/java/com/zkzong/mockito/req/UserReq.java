package com.zkzong.mockito.req;

import lombok.Data;

@Data
public class UserReq {

    private String userName;
    private Integer age;
    private String address;

}
