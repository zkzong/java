package com.zkzong.http.pojo;

import lombok.Data;

/**
 * @Author: zong
 * @Date: 2019.1.17
 */
@Data
public class User {
    private String userName;
    private Integer age;
}
