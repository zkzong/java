# java
[![Build Status](https://travis-ci.org/zkzong/java.svg?branch=master)](https://travis-ci.org/zkzong/java)
[![CircleCI](https://circleci.com/gh/zkzong/java.svg?style=svg)](https://circleci.com/gh/zkzong/java)
[![codecov](https://codecov.io/gh/zkzong/java/branch/master/graph/badge.svg)](https://codecov.io/gh/zkzong/java)

数据库：
[db](README-db.md)
- mysql
- redis
- mongodb
- es
- hbase

dubbo：
[dubbo](README-dubbo.md)

fastdfs：
[fastdfs](README-fastdfs.md)

javabasic：
[javabasic](README-javabasic.md)

job-scheduler：
[job-scheduler](README-job-scheduler.md)

library：
[library](README-library.md)

log：
[log](README-log.md)

消息队列：
[mq](README-mq.md)

spring：
[spring](README-spring.md)
- spring
- spring boot
- spring cloud

thirdparty：
[thirdparty](README-thirdparty.md)
