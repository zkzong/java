package com.zkzong.sb2.model;

import lombok.Data;

@Data
public class A<T> {
    private T t;
}
