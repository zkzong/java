package com.zkzong.map.service;

import java.math.BigDecimal;

public interface CarService {
    BigDecimal price();
}
