package com.zkzong.sb.security2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Zong on 2017/5/31.
 */
@SpringBootApplication
public class Security2Application {
    public static void main(String[] args) {
        SpringApplication.run(Security2Application.class, args);
    }
}
