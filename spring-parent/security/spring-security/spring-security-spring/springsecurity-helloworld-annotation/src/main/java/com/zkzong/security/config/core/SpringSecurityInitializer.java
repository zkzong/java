package com.zkzong.security.config.core;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Zong on 2017/5/20.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
