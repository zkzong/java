package com.atguigu.aop;

/**
 *
 * @Author: zkzong
 * @Date: 2019/9/26
 */
public class MathCalculator {

    public int div(int i, int j) {
        System.out.println("MathCalculator...div...");
        return i / j;
    }

}
