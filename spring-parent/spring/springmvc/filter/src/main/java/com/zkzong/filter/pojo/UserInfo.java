package com.zkzong.filter.pojo;

/**
 * Created by Zong on 2017/1/14.
 */
public class UserInfo {
    private String sex;
    private Integer age;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
