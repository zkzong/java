package com.zkzong.mybatis.service;

/**
 * @Author: zkzong
 * @Date: 2018.11.14
 */
public interface TxService {

    void insertTx(String name, int age);

}
