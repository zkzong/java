package com.zkzong.mybatis.service;

import com.zkzong.mybatis.domain.User;

/**
 * Created by Zong on 2017/5/31.
 */
public interface UserService {

    int insertUser(String name, int age);

}
