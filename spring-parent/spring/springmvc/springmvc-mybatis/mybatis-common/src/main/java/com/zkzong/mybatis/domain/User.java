package com.zkzong.mybatis.domain;

import lombok.Data;

/**
 * @Author: zkzong
 * @Date: 2018.11.14
 */
@Data
public class User {
    private int id;
    private String name;
    private int age;
}
