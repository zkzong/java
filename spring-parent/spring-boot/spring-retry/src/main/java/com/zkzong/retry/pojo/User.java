package com.zkzong.retry.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Zong on 2017/7/12.
 */
@Data
@AllArgsConstructor
public class User {
    private String name;
    private Integer age;
}
