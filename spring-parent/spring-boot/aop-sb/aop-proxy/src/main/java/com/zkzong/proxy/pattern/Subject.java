package com.zkzong.proxy.pattern;

public interface Subject {
    void request();
    void hello();
}
