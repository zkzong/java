package com.zkzong.sb.log4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Zong on 2017/5/2.
 */
@SpringBootApplication
public class Log4jApplication {
    public static void main(String[] args) {
        SpringApplication.run(Log4jApplication.class, args);
    }
}
