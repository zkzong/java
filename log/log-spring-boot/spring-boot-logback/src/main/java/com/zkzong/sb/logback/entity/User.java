package com.zkzong.sb.logback.entity;

import lombok.Data;

/**
 * @Author: Zong
 * @Date: 2018/12/18
 */
@Data
public class User {
    private Integer id;
    private String name;
    private Integer age;
}
