# db

## 1. redis

【注意】和spring集成时注意jedis、spring和spring-data-redis的版本问题

### 1.1 redis_cache
通过aop使用redis实现缓存

## 2. mongodb

## 3. elasticsearch

> **注意版本**
> Spring Boot 2.1.6
> ElasticSearch 5.6.0

## 4. hbase